module.exports = function(services){
    var express = require('express');
    var router = express.Router();

    /* GET home page. */
    router.get('/', function(req, res, next) {
        res.render('index', {
            title: 'trax.am status',
            services: services
        });
    });
    return router;
};
