⚠️ This project has been discontinued.

# 📦 statuspage
This node.js package offers a small web page that shows the current status of your services.

## ❓ How it works
Every service that should be monitored must send a http PUSH request to this webserver. Services are considered as "down" of they do not send a PUSH request in 30 seconds.

## ⚙️ Configuration
Create a `services.json` file at the root of this project and copy the following example into the file:
```json
{
    "services": [
        {
            "id": "website",
            "name": "Website",
            "token": "some-super-secret-token",
            "website": "https://example.com",
            "timeout": 90000
        },
        {
            "id": "some-other-service",
            "name": "Some other service (SOS)",
            "token": "another-secret-string",
            "website": "https://gitlab.com/my-name/my-project",
            "timeout": 30000
        }
    ]
}
```
In the `services` array, each entry represents a service that is displayed on statuspage's web interface.

| property name | what it does |
| ------------- | ------------ |
| `id`          | Specifies the identifier for a service which is used in the PUSH url. |
| `name`        | The name that will be displayed on the statuspage-webpage. |
| `token`       | A **secret** string that is used for authenticating a PUSH request. You might want to use something that is not that easy to guess. |
| `website`     | A link to a website about the service. |
| `timeout`     | Services will be marked as down after this interval of time (in milliseconds). This is not implemented yet. |

## 📝 Copyright and license
Copyright (c) 2018 traxam. Licensed under the MIT license. A full copy of the license can be found in the `LICENSE` file.
