const express = require('express');

module.exports = function(services){
    let router = express.Router();

    // post service heartbeat
    router.post('/:service', function(req, res, next) {
        let service = services.getServiceById(req.params.service);
        let tokenMatches = service.token === req.get('Authorization');
        if (!tokenMatches) {
            res.sendStatus(401);
            return;
        }
        service.lastSeen = new Date();
        res.sendStatus(200);
    });
    return router;
};
